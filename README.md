# Snakemake live demo

This repository hosts the template for the official Snakemake live demo.

## Steps

### Step 1

* rule bwa for sample A
* add wildcards
* add conda env

### Step 2

* rule sort

### Step 3

* rule index

### Step 4

* rule call

### Step 5

* rule stats

### Step 6

* create report
* annotate output file

### Step 7

* mention log files
* mention config file
* mention sample sheets
